# Caelum-Pulsar

### Project Status
As all the elements of the **NoxNivi** 'set', this is a Work In Progress,
therefore development pace is slow.

## Description
Default cursors for the NoxNivi set.
Intended to be used with dark themes. NoxNivi default theme is black thus this
fits.

## Screenshots
![Caelum-Pulsar cursor theme](/screenshots/caelum-pulsar.png "Caelum-Pulsar")*Caelum-Pulsar*

## Installation
**General instructions**

Download the the compressed package, decompress it and copy the Caelum-Pulsar
folder to ~/.icons/
Then select the Caelum-Pulsar theme in your cursor preference settings.

**Arch Linux**

If user is running Arch or any Arch derived distro, can add the NoxNivi
repository and install it using `pacman`:
```
sudo pacman -S caelum-pulsar
```
Then select the Caelum-Pulsar cursor theme in your cursor preference settings.


## Usage
Install, select in cursor preferences and enjoy.

## Support

## License
THis software is released under the GPLv3 license. A copy of said license is provided in this folder.
